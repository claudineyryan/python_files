rios = [
    {'Nilo': 'Egito'}, 
    {'Amazonas': 'Brasil'}, 
    {'Tiete': 'São Paulo'}, 
    {'Rio Mimbo': 'Mimbo'}
]

for rio in rios:
    for chave, valor in rio.items():
        print('O', chave, 'corre pelo', valor)
        print('Rio:', chave)
        print('País:', valor)

