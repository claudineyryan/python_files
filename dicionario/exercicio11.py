pets = [
    {'tipo': 'cachorro', 'nome_dono': 'Marcos'},
    {'tipo': 'papagaio', 'nome_dono': 'Fernando'},
    {'tipo': 'peixe', 'nome_dono': 'Gustavo'},
    {'tipo': 'gato', 'nome_dono': 'Carlos'},
    {'tipo': 'elefante', 'nome_dono': 'João'},
    {'tipo': 'cobra', 'nome_dono': 'James'},
]

for pet in pets:
    print('-'*38)
    for chave, valor in pet.items():
        print(chave,':',valor)