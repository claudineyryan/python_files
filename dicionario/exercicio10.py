pessoas = [
    {'nome': 'João', 'sobrenome': 'Marcos', 'idade': '18', 'cidade': 'Amarante-PI'},
    {'nome': 'Fernanda', 'sobrenome': 'Silva', 'idade': '56', 'cidade': 'Angical-PI'},
    {'nome': 'Gustavo', 'sobrenome': 'Santos', 'idade': '34', 'cidade': 'São Pedro-PI'}
]

for pessoa in pessoas:
    print('-'*38)
    for chave, valor in pessoa.items():
        print(chave,':',valor)