pessoas = [
    {'Nome': 'João', 'Número Favorito': 12},
    {'Nome': 'Carlos', 'Número Favorito': 56},
    {'Nome': 'Miguel', 'Número Favorito': 89},
    {'Nome': 'Rafael', 'Número Favorito': 32},
    {'Nome': 'Fernanda', 'Número Favorito': 123}
]

for pessoa in pessoas:
    print('-'*30)
    for chave, valor in pessoa.items():
        print(chave,':',valor)