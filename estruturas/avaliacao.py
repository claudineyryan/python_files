import os
from estrutura_dados import Estrutura

estrutura = Estrutura()

def menu():
    """Método responsável por controlar o fluxo de dados do sistema"""
    while True:
        os.system('clear')
        print('----------------------------------------')
        print('#### MENU #### \n 1. Cadastrar Aluno \n 2. Listar Alunos \n 3. Excluir Aluno \n 4. Registrar Nota \n 5. Sair')
        print('----------------------------------------')
        
        opcao = input('Digite o número da opção selecionada: ')        
        if opcao == '1':
            cadastrar()
        elif opcao == '2':
            listar()
        elif opcao == '3':
            excluir()
        elif opcao == '4':
            registrar_nota()
        elif opcao == '5':
            print('Bjos! S2')
            break

        print()
        if(input('Deseja realizar outra operação? (y,n)').lower() == 'n'):
            break


def cadastrar():
    os.system('clear')
    # Quando o usuário digitar o nome do aluno e pressionar a tecla enter, o valor informado será armazenado na variável aluno
    aluno = input('Digite o nome do aluno: ')

    if estrutura.cadastrar_alunos(aluno):
        print('Aluno cadastrado com sucesso!')
    else: 
        print('Aluno já está cadastrado!')
    
    # Adicione o aluno à lista de alunos. Ao final exiba uma mensagem de sucesso!
    # Não deve permitir que adicione um aluno que já está cadastrado. Exibe uma mensagem informando que o erro aconteceu e volte ao MENU    

def listar():
    os.system('clear')
    # Exiba o nome de cada aluno da lista assim como sua respectiva nota e disciplina. Deixe para exibir os valores de nota e disciplina somente quando tiver implementado o método registrar_nota()
    # Caso a nota não tenha sido registrada para um determinado aluno, exiba a mensagem "Nota não registrada"    
    for aluno_dict in estrutura.listar_alunos():
        print('-'*20)
        print('Aluno:', aluno_dict['aluno'])
        print('Disciplina:', aluno_dict['disciplina'])
        print('Nota:', aluno_dict['nota'])


def excluir():
    os.system('clear')
    # Quando o usuário digitar o nome do aluno e pressionar a tecla enter, o valor informado será armazenado na variável aluno
    aluno = input('Digite o nome do aluno que deseja excluir: ')

    if estrutura.excluir_alunos(aluno):
        print('Aluno excluido com sucesso!')
    else:
        print('Erro ao deletar!')

    #   Remova o aluno da lista de alunos. Ao final exiba uma mensagem de sucesso!
    #   Caso o aluno não esteja cadastrado, exiba a mensagem "Aluno não cadastrado"

def registrar_nota():
    os.system('clear')
    # Quando o usuário digitar o nome do aluno e pressionar a tecla enter, o valor informado será armazenado na variável aluno
    aluno = input('Digite o nome do aluno: ')
    disciplina = str(input('Digite a disciplina: '))
    nota = float(input('Digite a nota da disciplina: '))

    # Solicite ao usuário para informar os dados de disciplina e nota
    if estrutura.registrar_notas(aluno, disciplina, nota):
        print('Notas e disciplinas cadastradas com sucesso!')
    else:
        print('Erro ao cadastrar, aluno não existente!')
    
    # Adicione a disciplina e nota referente ao aluno informado ao dicionário notas    

menu()