from classes import Fila

# 1. Crie uma Fila e adicione pelo menos 3 elementos
fila = Fila()
fila.add('1')
fila.add('2')
fila.add('3')

# 2. Verifique se o proximo elemento da Fila corresponde ao primeiro elemento adicionado
if fila.peek() == None:
    print('Não exite elemento na lista!')
else:
    print('Priemiro elemento:',fila.peek())

# 3. Remove o próximo elemento da Fila e verifica se o elemento removido corresponde ao primeiro elemento adicionado
if fila.is_empty():
    print('Não exite elemento na lista!')
else:
    print(fila.remove(), 'foi removido!')

# #4. Verifica se o próximo elemento da Fila corresponde ao segundo elemento inserido na Fila
if fila.peek() == None:
    print('Não exite elemento na lista!')
else:
    print('Segundo elemento:', fila.peek())

# # 5. Teste se o is_empty está retornando o valor correto
print('Está vazia: ', fila.is_empty())

# # 6. Remova todos os elementos da Fila
while fila.is_empty() == False:
    print(fila.remove(), 'foi removido!')

# # 7. Teste se o is_empty está retornando o valor correto
print('Está vazia: ', fila.is_empty())

# # 8. Verifique se o método peek() está retornando o valor correto
if fila.peek() == None:
    print('Não exite elemento na lista!')
else:
    print(fila.peek())