#Listas
bicicletas = ['trek', 'cannondale', 'redline', 'specialize']
print(bicicletas)

#Acessando os elementos de uma lista
print('indice 0 =>', bicicletas[0])
print('indice 1 =>', bicicletas[1])
print('indice 2 =>', bicicletas[2])

#Alterar o valor do elemento do índice 3 da lista
bicicletas[3] = 'houston'
print(bicicletas)

#Adiciona uma nova bicicleta ao final da lista
bicicletas.append('especialize')
print(bicicletas)

#Adiciona uma nova bicicleta na posição 1 da lista
bicicletas.insert(0, 'monarq')
print(bicicletas)

#Remove o ultimo elemento da lista
bicicletas.pop()
print(bicicletas)

#Após exclusão ele pode ser retornado
bike_removida = bicicletas.pop()
print('Bike removida => ', bike_removida)
print(bicicletas)

#Removendo um elemento por meio do valor
bicicletas.remove('trek')
print(bicicletas)

#Busca um elemento a partir do valor armazenado
index = bicicletas.index('monarq')
print(f'Elemento encontrado na posição => {index}')

#Busca a quantidade de vezes que o elemento aparace
#print('Quantidade de vezes que aparace =>' bicicletas.count('redline')) #Deu error