cidades = {
    'Amarante': {'país': 'Brasil', 'população': '1,2 Milhões', 'curiosidade': 'sei la, mas é curioso'},
    'Washington': {'país': 'EUA', 'população': '6,2 Milhões', 'curiosidade': 'sei la, mas é curioso tambem'},
    'Vancouver': {'país': 'Canada', 'população': '9,2 Milhões', 'curiosidade': 'ainda mais curioso'}
}

for chave, valor in cidades.items():
    print(20*'-',chave,'-'*20)
    for key, value in valor.items():
        print(key,':',value)