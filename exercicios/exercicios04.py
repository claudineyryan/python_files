lugares = ['França', 'EUA', 'Mexico', 'Japão', 'Canada', 'Italia']
print('')

print('Lista inicial: ', lugares)
print('')

print(f'Ordem alafabetica: {sorted(lugares)}')
print('')

print('Lista inicial: ', lugares)
print('')

print(f'Ordem alafabetica inversa: {sorted(lugares, reverse=True)}')
print('')

print('Lista inicial: ', lugares)
lugares.reverse()
print('Lista inicial reversa: ', lugares)
lugares.reverse()
print('Lista inicial: ', lugares)

print('')

lugares.sort()
print('Lista inicial alfabetica permanente: ', lugares)
lugares.reverse()
print('Lista alfabetica inversa: ', lugares)