# Paciente e Médico
import os
from classes import Fila

fila_atendimento = Fila()

#Vamos pedir o nome do paciente e idade

def menu():    
    while True:
        
        os.system('clear')
        print('----------------------------------------')
        print('#### MENU #### \n 1. Adicionar paciente \n 2. Chamar próximo \n 3. Sair')
        print('----------------------------------------')
        
        opcao = input('Digite o número da opção selecionada: ')        
        if opcao == '1':
            marcar_chegada()
        elif opcao == '2':
            chamar()
        elif opcao == '3':
            print('Bjos! S2')
            break

        print()
        if(input('Deseja realizar outra operação? (y,n)').lower() == 'n'):
            break

# Método responsável por adicionar um novo paciente à fila
def marcar_chegada():
    os.system('clear')
    # TODO - 1. Solicite ao usuário que informe o nome do paciente que deseja adicionar à Fila
    nome = str(input('Informe o nome do paciente: '))
    idade = int(input('Informe a idade do paciente: '))
    paciente = {
        'nome': nome,
        'idade': idade
    }

    fila_atendimento.add(paciente)
    print('\nPaciente adicionado com sucesso!')

    # TODO - 3. Caso o paciente não esteja na Fila, adicione-o à fila de atendimento e exiba uma mensagem de sucesso

def chamar():
    os.system('clear')
    # TODO - 1. Verifique se existe algum paciente na fila. Caso não tenha, exiba uma mensagem.
    if fila_atendimento.is_empty():
        print('Não existe pacientes na fila!')
    else:
        print('Proximo paciente: ', fila_atendimento.peek()['nome'])
        print('Idade do paciente: ', fila_atendimento.peek()['idade'])
        print('\n', fila_atendimento.remove()['nome'], 'Está sendo atendido!')

    # TODO - 2. Caso existam pacientes na lita, remova o próximo elemento da Fila.

    # TODO - 3. Exiba uma mensagem de sucesso informando que o paciente removido da Fila está sendo atendido

menu()