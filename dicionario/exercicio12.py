favorite_places = {
    'João': ['Miami', 'Brasil', 'Berlim'],
    'Fernanda': ['Las Vegas', 'Saco da Cachoeira', 'EUA'],
    'James': ['Japão', 'Mimbo', 'Canada']
}

for key, lugares in favorite_places.items(): 
    print('-'*38) 
    for lugar in lugares:
        print(key, '=> lugar:', lugar)