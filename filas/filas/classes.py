class Fila():

    """Fila: inicio <-- <-- <-- <-- <-- <-- final"""

    # Defina uma estrutura [lista ou dicionário] para armazenar os dados manipulados pela Fila.
    def __init__(self):
        self.dados = []

    # Método responsável por adicionar um elemento ao final da fila
    def add(self, elemento):
        self.dados.append(elemento)

    # Método responsável por remover um elemento do inicio da fila
    def remove(self):
        return self.dados.pop(0)

    # Método que verifica se uma fila está vazia
    def is_empty(self):
        return self.dados == []

    # Método que retorna o elemento do inicio da fila sem removê-lo da lista
    def peek(self):
        if self.is_empty():
            return None
        return self.dados[0]