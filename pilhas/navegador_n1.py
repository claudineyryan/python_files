import os
from classes import Pilha

sites_visitados = Pilha()
site_atual = ''

def menu():    
    while True:
        
        os.system('clear')
        print('----------------------------------------')
        print('#### MENU #### \n 1. Ir para novo site \n 2. Voltar \n 3. Sair')
        print('----------------------------------------')
        
        opcao = input('Digite o número da opção selecionada: ')        
        if opcao == '1':
            ir()
        elif opcao == '2':
            voltar()
        elif opcao == '3':
            print('Bjos! S2')
            break

        print()
        if(input('Deseja realizar outra operação? (y,n)').lower() == 'n'):
            break

# Método responsável por abrir uma novo site
def ir():
    os.system('clear')

    global site_atual
    # TODO - 3. Caso algum site esteja sendo acessado [site_atual != ''], adicione site_atual à pilha de sites visitados   
    if site_atual != '':
        sites_visitados.push(site_atual) 

    # TODO - 1. Solicite ao usuário que informe o site que deseja acessar
    site_atual = str(input('Informe o site desejado: '))
        
    # TODO - 4. Exiba uma mensagem de sucesso informando o endereço do site que está sendo exibido
    print('\nSite cadastrado com sucesso!')
    print('\nSITE ATUAL:', site_atual)

def voltar():
    os.system('clear')
    global site_atual
    # TODO - 1. Verifique se existe algum site na Pilha de sites visitados. Caso não tenha, exiba uma mensagem.
    if sites_visitados.is_empty():
        print('Não existe nenhum site!')
    else:
        site_atual = sites_visitados.pop()
        print('\nVocê voltou um site!')
        print('\nSITE ATUAL:', site_atual)

    # TODO - 2. Remova o último site acessado da Pilha, atribua-o à variável site_atual

    # TODO - 3. Exiba uma mensagem de sucesso informando o endereço do site que está sendo exibido
    

menu()