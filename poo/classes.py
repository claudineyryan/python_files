class Dog():
    """Classe para representar Cachorro"""

    def __init__(self, nome, raca, idade, vacinado):
        self.nome = nome
        self.raca = raca
        self.idade = idade
        self.vacinado = vacinado
        """Esse metodo recebe self como parametro sempre, e sempre
        inicia a classe, tambem declaramos suas propriedades aqui"""

    def latir(self):
        print(self.nome, 'disse: au-au-au')

    def fazer_aniversario(self):
        self.idade += 1
