import os

alunos = []
notas = {}

def menu():
    """Método responsável por controlar o fluxo de dados do sistema"""
    while True:
        os.system('clear')
        print('----------------------------------------')
        print('#### MENU #### \n 1. Cadastrar Aluno \n 2. Listar Alunos \n 3. Excluir Aluno \n 4. Registrar Nota \n 5. Média\n 6. Sair')
        print('----------------------------------------')
        
        opcao = input('Digite o número da opção selecionada: ')        
        if opcao == '1':
            cadastrar(alunos)
        elif opcao == '2':
            listar(alunos, notas)
        elif opcao == '3':
            excluir(alunos)
        elif opcao == '4':
            registrar_nota(alunos, notas)
        elif opcao == '5':
            media(alunos, notas)
        elif opcao == '6':
            print('Bjos! S2')
            break

        print()
        if(input('Deseja realizar outra operação? (y,n)').lower() == 'n'):
            break

def cadastrar(lista):
    os.system('clear')
    # Quando o usuário digitar o nome do aluno e pressionar a tecla enter, o valor informado será armazenado na variável aluno
    
    aluno = str(input('Digite o nome do aluno: '))

    if lista.count(aluno) > 0:
        print('Aluno já está cadastrado!')
    else:
        lista.append(aluno)
        print('Aluno cadastrado com sucesso!')

    # Adicione o aluno à lista de alunos. Ao final exiba uma mensagem de sucesso!
    # Não deve permitir que adicione um aluno que já está cadastrado. Exibe uma mensagem informando que o erro aconteceu e volte ao MENU    

def listar(lista, dicio):
    os.system('clear')

    print('---------------Lista de Alunos----------------')
    for aluno in lista: #Listando cada aluno.
        print(aluno)

    print('')
    print('---------------Lista de Disciplinas e Notas----------------')
    # Exiba o nome de cada aluno da lista assim como sua respectiva nota e disciplina. Deixe para exibir os valores de nota e disciplina somente quando tiver implementado o método registrar_nota()
    for nome_aluno, dicipli_nota in dicio.items():
        for disciplina_lista in dicipli_nota:
            for disciplina_key, nota in disciplina_lista.items():
                print(nome_aluno,'=>',disciplina_key,':',nota)

    # Caso a nota não tenha sido registrada para um determinado aluno, exiba a mensagem "Nota não registrada"    

def excluir(lista):
    os.system('clear')
    # Quando o usuário digitar o nome do aluno e pressionar a tecla enter, o valor informado será armazenado na variável aluno
    aluno = str(input('Digite o nome do aluno que deseja excluir: '))

    if lista.count(aluno) > 0:
        lista.remove(aluno)
        print(aluno,'foi removido com sucesso!')
    else:
        print('Esse aluno não exite!')  

    #   Remova o aluno da lista de alunos. Ao final exiba uma mensagem de sucesso!
    #   Caso o aluno não esteja cadastrado, exiba a mensagem "Aluno não cadastrado"

def registrar_nota(lista, dicio):
    os.system('clear')
    # Quando o usuário digitar o nome do aluno e pressionar a tecla enter, o valor informado será armazenado na variável aluno
    aluno = str(input('Digite o nome do aluno: '))
    escolha = True

    while escolha:
        if aluno in lista:
            if aluno in dicio:
                # Solicite ao usuário para informar os dados de disciplina e nota
                disciplina = str(input('Informe a disciplina: '))
                nota = float(input('Informe a nota da disciplina: '))

                # Adicione a disciplina e nota referente ao aluno informado ao dicionário notas
                nova_disciplina = dicio[aluno]
                nova_disciplina.append({disciplina: nota})

                print('\nCadastro de disciplina e nota com sucesso!\n')
                print('Deseja resistrar mais uma nota? \n1 - Sim(s) \n2 - Não(n)')
                escolha = input('Informe: ')
                if escolha == 's':
                    escolha == True
                    os.system('clear')
                else:
                    escolha == False
            else:
                # Solicite ao usuário para informar os dados de disciplina e nota
                disciplina = str(input('Informe a disciplina: '))
                nota = float(input('Informe a nota da disciplina: '))
                
                # Adicione a disciplina e nota referente ao aluno informado ao dicionário notas
                dicio.update({aluno: [{disciplina: nota}]})  

                print('\nCadastro de disciplina e nota com sucesso!\n')
                print('Deseja resistrar mais uma nota? \n1 - Sim(s) \n2 - Não(n)')
                escolha = input('Informe: ')
                if escolha == 's':
                    escolha == True
                    os.system('clear')
                else:
                    escolha == False
            
        else:
            print('\nAluno não está cadastrado!')  

def media(lista, dicio):
    os.system('clear')
    # Quando o usuário digitar o nome do aluno e pressionar a tecla enter, o valor informado será armazenado na variável aluno
    aluno = str(input('Digite o nome do aluno: '))

    if aluno in lista:
        if aluno in dicio:
            media = 0.0
            disciplina_nota = dicio[aluno]
            for nome_aluno in disciplina_nota:
                for disciplina, nota in nome_aluno.items():
                    media = media + nota
            print('Média =>',aluno,':',media/len(dicio[aluno]),'pontos anual')
        else: 
            print('Aluno não possui notas!')
    else:
        print('Aluno não cadastrado!')

menu()