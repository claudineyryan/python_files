class Pilha():

    """
    Base da Pilha <--  |0|1|2|3|4|5|6|7|8| --> Topo da Pilha
    """

    # Defina uma estrutura [lista ou dicionário] para armazenar os dados manipulados pela Pilha.
    def __init__(self):
        self.dados = []
    
    # Método responsável por adicionar um novo elemento no topo da Pilha
    def push(self, elemento):
        self.dados.append(elemento) #Adiciona no final da lista

    # Método responsável por remover e retornar o elemento que está no topo da pilha
    def pop(self):
        if self.is_empty():
            return None
        return self.dados.pop()

    # Método que verifica se a Pilha está vazia
    def is_empty(self):
        return len(self.dados) == 0  

    # Método que retorna o elemento que está no topo da pilha sem removê-lo
    def top(self):
        if self.is_empty(): #Caso a lista esteja vazia
            return None
        return self.dados[-1] #Retorna o ultimo elemento da lista
        
         
