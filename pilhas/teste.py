from classes import Pilha

# 1. Crie uma pilha e adicione pelo menos 3 elementos
pilha = Pilha()
pilha.push('Carlos')
pilha.push('James')
pilha.push('Pedro')

# 2. Verifique se o topo da pilha corresponde ao último elemento adicionado
print('Ultimo elemento da lista: ', pilha.top())

# 3. Remove o elemento do top e verifica se o elemento removido corresponde ao último elemento adicionado 
print(pilha.pop(), 'é o ultimo elemento e foi removido!')

#4. Verifica se o elemento que está no topo corresponde ao penúltimo elemento inserido na Pilha
if pilha.top() == None:
    print('Lista ESTÁ vazia!')
else:
    print(pilha.top(), 'é o penultimo elemento da lista!')

# 5. Teste se o is_empty está retornando o valor correto
if pilha.is_empty():
    print('Lista está vazia!')
else:
    print('Lista não está vazia!')

# 6. Remova todos os elementos da Pilha
while not pilha.is_empty():
    print(pilha.pop(), 'foi removido da lista!')

# 7. Teste se o is_empty está retornando o valor correto
if pilha.is_empty():
    print('Lista ESTÁ vazia!')
else:
    print('Lista NÃO está vazia!')

# 8. Verifique se o método top() está retornando o valor correto
if pilha.top() == None:
    print('Lista ESTÁ vazia!')
else:
    print(pilha.top(), 'é o penultimo elemento da lista!')