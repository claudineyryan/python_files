class Estrutura:

    def __init__(self):
        self.dados = []
        self.notas = {}

    # Método deve inserir um aluno à lista alunos;
    # Deve retornar True quando inserir com sucesso;
    # Deve retornar False quando o aluno já constar na Lista;
    def cadastrar_alunos(self, aluno):    
        if aluno in self.dados:
            return False
        else:
            self.dados.append(aluno)
            return True

    # Deve retornar uma 'Estrutura' já com alunos e notas;
    def listar_alunos(self):
        retorno = []
        for aluno in self.dados: #Ele pesquisa se o aluno esta na lista
            _aluno = { #Criamos disciplina e nota já com valores padrão
                'aluno': aluno,
                'disciplina': '',
                'nota': 0
            }
            if aluno in self.notas: #Ele pesquisa se o aluno da lista esta no dicionario;
                _aluno['disciplina'] = self.notas[aluno]['disciplina'] #Ele cria a chave disciplina
                _aluno['nota'] = self.notas[aluno]['nota'] #Ele cria a chave nota
            retorno.append(_aluno)

        return retorno

    # Deve excluir o aluno da lista;
    # Em caso de sucesso deve retornar True;
    # Em caso de erro deve retornar False;
    def excluir_alunos(self, aluno):
        if aluno in self.dados: #Existe esse aluno, entao apaga;
            self.dados.remove(aluno)
            if aluno in self.notas: #Existe esse aluno no dicionario de notas?
                del(self.notas[aluno]) #Entao apaga
                return True
        else:
            return False

    # Deve registrar a nota do aluno;
    # Em caso de sucesso deve retornar True;
    # Em caso de erro deve retornar False;
    def registrar_notas(self, aluno, disciplina, nota):
        if aluno in self.dados: #Aluno existe na lista para ser colocado as notas?
            self.notas[aluno] = {
                'disciplina': disciplina,
                'nota': nota
            }
            return True
        else:
            return False      
