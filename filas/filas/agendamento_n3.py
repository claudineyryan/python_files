# Paciente e Médico
import os
from classes import Fila

fila_atendimento = Fila()
filas = {}

#Vamos pedir o nome do paciente e idade

def menu():    
    while True:
        
        os.system('clear')
        print('----------------------------------------')
        print('#### MENU #### \n 1. Adicionar paciente \n 2. Chamar próximo \n 3. Sair')
        print('----------------------------------------')
        
        opcao = input('Digite o número da opção selecionada: ')        
        if opcao == '1':
            marcar_chegada()
        elif opcao == '2':
            chamar()
        elif opcao == '3':
            print('Bjos! S2')
            break

        print()
        if(input('Deseja realizar outra operação? (y,n)').lower() == 'n'):
            break

# Método responsável por adicionar um novo paciente à fila
def marcar_chegada():
    os.system('clear')
    # TODO - 1. Solicite ao usuário que informe o nome do paciente que deseja adicionar à Fila
    nome_medico = str(input('Informe o nome do medico: '))
    paciente = str(input('Informe o nome do paciente: '))
    
    if nome_medico in filas:
        filas[nome_medico].add(paciente)
        print('\nPaciente adicionado com sucesso!')
        
    else:
        filas[nome_medico] = Fila()
        filas[nome_medico].add(paciente)
        print('\nPaciente e medico adicionados com sucesso!')

    # TODO - 3. Caso o paciente não esteja na Fila, adicione-o à fila de atendimento e exiba uma mensagem de sucesso

def chamar():
    os.system('clear')
    # TODO - 1. Verifique se existe algum paciente na fila. Caso não tenha, exiba uma mensagem.
    nome_medico = str(input('Informe o nome do medico: '))
    
    if nome_medico in filas:
        if filas[nome_medico].is_empty():
            print('Não existe pacientes na fila!')
        else:
            print('Proximo paciente: ', filas[nome_medico].peek())
            print(filas[nome_medico].remove(), 'Está sendo atendido!')

    # TODO - 2. Caso existam pacientes na lita, remova o próximo elemento da Fila.

    # TODO - 3. Exiba uma mensagem de sucesso informando que o paciente removido da Fila está sendo atendido

menu()