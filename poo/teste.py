from classes import Dog

josefa = Dog('Josefa', 'viralata', 8, False)

print(josefa.nome)
print(josefa.raca)
print(josefa.idade)
print(josefa.vacinado)

josefa.nome = 'Josefine'
print(josefa.nome)

print('Daqui 5 anos a idade vai ser', josefa.idade + 5)

rex = Dog('rex', 'pastor', 1, True)
rex.latir()
rex.fazer_aniversario()