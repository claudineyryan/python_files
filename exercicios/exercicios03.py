convidados = ['Maria', 'João', 'Klebio', 'Felipe', 'Miguel', 'Betina', 'Petronio']

#Letra (a)
for convite in convidados:
    print(f'{convite} você está sendo convidado para o meu aniversário ;)')
print('')

#Letra (b)
convidado_removido = 'Maria'
indice = convidados.index(convidado_removido)
convidados[indice] = 'Fernanda'
print(f'O amigo(a) {convidado_removido} foi substituído por {convidados[indice]}' )
print('')

#Letra (c)
convidados.insert(0, 'Bento')
metade = int(len(convidados)/2)
convidados.insert(metade, 'Cris')
convidados.append('Estephe')
print(convidados)
print('')
for convite in convidados:
    print(f'{convite} você está sendo convidado para o meu aniversário ;)')
print('')

#Letra (d)
while len(convidados) > 2:
    tchau = convidados.pop()
    print(f'{tchau} eu sinto muito por não poder mais convidá-lo(a) para a festa.')
print('')
for convite in convidados:
    print(f'{convite} você ainda está convidado para o meu aniversário ;)')
print('')